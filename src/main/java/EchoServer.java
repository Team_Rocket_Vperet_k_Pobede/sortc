import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class EchoServer {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.print("Usage: java EchoServer 6666");
            System.exit(1);
        }
        int portNumber = Integer.parseInt(args[0]);
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(Integer.parseInt(args[0]));
        } catch (IOException e) {
            System.out.println("exception caught when trying to listen on port" + portNumber + "or listening to a connection");
            System.out.println(e.getMessage());

        }
        Manager manager = new Manager(new ArrayList<Integer>());
        while (true) {
            try {

                if (serverSocket != null) {
                    manager.addClient(serverSocket.accept());
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("accept failed: 6666");
                System.exit(-1);
            }
        }
    }
}

