import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Manager {

    private ArrayList  <Integer> array;
    public List <Socket> clients;

    public Manager(ArrayList<Integer> array) {
        this.array = array;
    }


    public void addClient(Socket client) {
        clients.add(client);
        tryToStartSort();
    }

    private void tryToStartSort() {
        if (clients.size() < 5) {
            return;
        }
        try {
            int amount = clients.size();
            int onePart = array.size() / amount;
            int pointer = 0;
            for (Socket client : clients) {
                ArrayList<Integer> newArray = (ArrayList<Integer>) array.subList(pointer, pointer + onePart);
                pointer += onePart;
                PrintWriter printWriter = new PrintWriter(client.getOutputStream(), true);
                for (Integer integer : newArray) {
                    printWriter.println(integer);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
