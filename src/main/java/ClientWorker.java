

import java.lang.*;
import java.io.*;
import java.net.*;

public class ClientWorker extends Thread {
    private Socket client;
    private PrintWriter out;


    public ClientWorker(Socket client) {
        this.client = client;
        run();
    }

     private void init() {
        try {
            out = new PrintWriter(client.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   public void sendToClient(int array[]) {

    }

    public void run() {
        String line;
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            in = new BufferedReader(new
                    InputStreamReader(client.getInputStream()));
            out = new
                    PrintWriter(client.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("in or out failed");
            System.exit(-1);
        }

        while (true) {
            try {
                line = in.readLine();
//Send data back to client
                out.println(line);
            } catch (IOException e) {
                System.out.println("Read failed");
                System.exit(-1);
            }
        }
    }
}
