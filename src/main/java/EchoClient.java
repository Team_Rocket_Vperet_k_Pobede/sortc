
import java.io.*;
import java.net.*;

public class EchoClient {
    public static void main(String[] args) throws IOException {
        InetAddress address = InetAddress.getLocalHost();
        String hostName = address.getHostName();
        int portNumber = Integer.parseInt(args[0]);

        try (
                Socket echoSocket = new Socket(hostName, portNumber);
                PrintWriter out = new PrintWriter(echoSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));

                BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        ) {
            String userImput;
            while ((userImput = stdIn.readLine()) != null) {
                out.println(userImput);
               // System.out.println("массив" + in.readLine());          //здесь он читает из сокета свое эхо
                while (in.readLine()!=null){
                    System.out.println("массив" + in.readLine());
                }
            }                                                          //нам надо чтоб он побуквенно читал массив и из этого складывалась строка
        } catch (UnknownHostException e) {
            System.err.println("Don't know about the host" + hostName);
            System.exit(1);

        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Couldn't get I/O for the connection to" + hostName);
            System.exit(1);
        }
    }
}